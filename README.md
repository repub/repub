# repub

The Repub Reader Hardware

## What?

Repub is the electronics and hardware repository for the repub project.  The software layer is in [guv](https://gitlab.com/repub/guv)

Its currently based on the RPI compute 4 and a Gooddisplay e-paper display; though this may change to a less overpowered device in the future.
